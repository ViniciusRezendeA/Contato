//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

class DetalhesViewController: UIViewController {
    @IBOutlet weak var nomeLabel:UILabel!
    @IBOutlet weak var numeroLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    @IBOutlet weak var enderecoLabel:UILabel!
    
    public var contato: Contato?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.numero
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
    override func prepare (for segue: UIStoryboardSegue, sender: Any?){
            let detalhesViewController = segue.destination as! DetalhesViewController
        let contato = sender as! Contato
        detalhesViewController.contato = contato
        
        }
}
